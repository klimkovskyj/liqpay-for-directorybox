<?php
/**
 * Plugin Name: LiqPay for Directorybox 
 * Description: The plugin adds a LiqPay gateway to the theme of directorybox
 * Version: 0.1
 * Author: Ivanchenko Ivan
 * Author URI: ivanchenko.v6@gmail.com
 * Text Domain: lp-f-db
 */

// Plugin DIR:
if( !defined('LPFDB_DIR_PATH') ) {
    define('LPFDB_DIR_PATH', plugin_dir_path( __FILE__ ));
}

// Plugin URL:
if( !defined('LPFDB_DIR_URL') ) {
    define('LPFDB_DIR_URL', plugin_dir_url( __FILE__ ));
}

if( ! class_exists( 'WP_DP_PAYMENTS' ) ) {
    require_once( ABSPATH . 'wp-content/plugins/wp-directorybox-manager/payments/class-payments.php' );
}

require_once( LPFDB_DIR_PATH.'include/class-LiqPay-Gateway.php' );

global $liqpay_gateway;
$liqpay_gateway = new WP_DP_LiqPay_Gateway();

add_filter('wp_dp_payment_process', 'liqpay_payment_process', 10, 2);
function liqpay_payment_process($transaction_detail, $wp_dp_transaction_fields) {
	extract($wp_dp_transaction_fields);
	if ( $transaction_pay_method == 'WP_DP_LIQPAY_GATEWAY' && ! empty($wp_dp_transaction_fields) ) {
		global $liqpay_gateway;
		$liqpay_gateway->liqpay_process_request($wp_dp_transaction_fields);
	}
}
