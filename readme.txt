== Description ==
The plugin adds a LiqPay gateway to the theme of directorybox (requires the wp-directorybox-manager plugin).

== Installation ==
1. Put the plug-in folder into [wordpress_dir]/wp-content/plugins/
2. Go into the WordPress admin interface and activate the plugin
3. Optional: go to the options page and configure the plugin

== Changelog ==
Version 0.1
- Release
