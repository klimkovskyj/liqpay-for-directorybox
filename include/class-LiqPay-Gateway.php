<?php
class WP_DP_LiqPay_Gateway extends WP_DP_PAYMENTS 
{
	/* Url liqpay gateway */
	private $_gateway_url;
	/* Listener */
	private $_server_callback_url;
	/* Redirect url */
	private $_response_url;
	/* Public key of LiqPay */
	private $_public_key;
	/* Private key of LiqPay */
	private $_private_key;
	/* Currency */
	private $_currency;
	
	/* Constructor */
	public function __construct() {
		global $wp_dp_plugin_options, $gateways;
		$gateways['WP_DP_LIQPAY_GATEWAY'] = 'LiqPay';
		
		$this->_gateway_url = "https://www.liqpay.ua/api/3/checkout";
		$this->_server_callback_url = LPFDB_DIR_URL . 'include/LiqPay-Listner.php';
		$this->_response_url = home_url('/ad-new-listing/?package_id=5707&tab=activation');
				
		$wp_dp_gateway_options = get_option('wp_dp_plugin_options');
		$this->_currency = $wp_dp_gateway_options['wp_dp_currency_type'];
		$this->_public_key = $wp_dp_gateway_options['wp_dp_liqpay_public_key'];
		$this->_private_key = $wp_dp_gateway_options['wp_dp_liqpay_private_key'];
		
		if ( empty( $wp_dp_gateway_options['wp_dp_liqpay_gateway_logo'] ) ) { 
			$wp_dp_gateway_options['wp_dp_liqpay_gateway_logo'] = LPFDB_DIR_URL . 'images/liqpay-logo.png';
			update_option('wp_dp_plugin_options', $wp_dp_gateway_options);
		}
	}
	
	// LiqPay setting
	public function settings($wp_dp_gateways_id = '') {
		$on_off_option = array( "show" => __('LiqPay options on', 'lp-f-db'), "hide" => __('LiqPay options off', 'lp-f-db') );

		$wp_dp_settings[] = array(
			"name" => __('LiqPay Settings', 'lp-f-db'),
			"id" => "tab-heading-options",
			"std" => __('LiqPay Settings', 'lp-f-db'),
			"type" => "section",
			"options" => "",
			"parrent_id" => "$wp_dp_gateways_id",
			"active" => true,
		);

		$wp_dp_settings[] = array( "name" => __('Default Status', 'lp-f-db'),
			"desc" => "",
			"hint_text" => '',
			"label_desc" => __('If this switch will be OFF, no payment will be processed via LiqPay.', 'lp-f-db'),
			"id" => "liqpay_gateway_status",
			"std" => "on",
			"type" => "checkbox",
			"options" => $on_off_option
		);

		$wp_dp_settings[] = array("name" => __('Public Key', 'lp-f-db'),
			"desc" => "",
			"hint_text" => '',
			"label_desc" => __('Add your LiqPay Public Key here', 'lp-f-db'),
			"id" => "liqpay_public_key",
			"std" => "",
			"type" => "text"
		);

		$wp_dp_settings[] = array("name" => __('Private Key', 'lp-f-db'),
			"desc" => "",
			"hint_text" => '',
			"label_desc" => __('Add your LiqPay Private Key here', 'lp-f-db'),
			"id" => "liqpay_private_key",
			"std" => "",
			"type" => "password"
		);

		return $wp_dp_settings;
	}
	
	/**
     * encode_params
     *
     * @param array $params
     * @return string
     */
    private function encode_params($params) {
        return base64_encode(json_encode($params));
    }
	
	/**
     * get_signature
     *
     * @param array $params
     * @return string
     */
    public function get_signature($params) {
        $private_key = $this->_private_key;
        $json = $this->encode_params($params);
        $str = $private_key . $json . $private_key;
        $signature = base64_encode(sha1($str, 1));     
        return $signature;
    }
		
	// Request to liqpay
	public function liqpay_process_request($params = '') {
			global $post, $wp_dp_gateway_options, $wp_dp_form_fields;
			extract($params);
			
			// Title of order post-type 
			$wp_dp_package_title = ($transaction_package == 'Promotions') ? $transaction_package : get_the_title($transaction_package);
			//ID listing post-type
			$trans_item_id = ($transaction_package == 'Promotions') ? $listing_id : $trans_item_id;
			$currency = isset($this->_currency) && $this->_currency != '' ? $this->_currency : 'USD';
			// Parameters for request
			$params = array(
				'public_key'	 => $this->_public_key,
				'action'         => 'pay',
				'result_url'	 => $this->_response_url,
				'server_url'	 => $this->_server_callback_url,
				'amount'         => $transaction_amount,
				'currency'       => $currency,
				'description'    => $wp_dp_package_title,
				'order_id'       => $transaction_id, // ID transaction post-type
				'info'			 => $trans_item_id,
				'version'        => '3',
				'language'		 => 'uk',
				'sandbox'        => '1'
				);
				
			$data      = $this->encode_params($params);
			$signature = $this->get_signature($params);

			$output = '';			
			$output .= '<form name="LiqPayForm" id="liqpay-form1" action="' . $this->_gateway_url . '" method="post">  
                                               
                        <input type="hidden" name="data" value="' . $data . '" />
						<input type="hidden" name="signature" value="' . $signature . '" />
                        </form>';

			$output .= '<script>
					  	 jQuery("#liqpay-form1").submit(); 
					  </script>';
			echo $output;
		}
	
}
