<?php
include_once('../../../../wp-load.php');

// Payment transaction custom transaction post
if(!function_exists('wp_dp_update_transaction')){
	function wp_dp_update_transaction($wp_dp_trans_array = array(), $wp_dp_trans_id) {
		foreach ( $wp_dp_trans_array as $trans_key => $trans_val ) {
			update_post_meta($wp_dp_trans_id, "$trans_key", $trans_val);
		}
		$transaction_order_id = get_post_meta($wp_dp_trans_id, "wp_dp_transaction_order_id", true);
		if ( $transaction_order_id ) {
			update_post_meta($transaction_order_id, 'wp_dp_transaction_status', 'approved');
		}
	}
}

// Payment transaction custom post update 
if(!function_exists('wp_dp_update_post')){
	function wp_dp_update_post($id = '', $wp_dp_trans_id = '') {
		global $wp_dp_plugin_options;

		$wp_dp_trans_pkg = get_post_meta($wp_dp_trans_id, 'wp_dp_transaction_package', true);
		if ( $id == $wp_dp_trans_pkg ) {
			update_post_meta($wp_dp_trans_id, 'wp_dp_listing_ids', '');
		}
		// Assign Status of listing
		do_action('wp_dp_listing_add_assign_status', $id);

		wp_dp_update_order_inquiry_post($id);
	}
}

if(!function_exists('wp_dp_update_order_inquiry_post')){
	function wp_dp_update_order_inquiry_post($order_id = '') {
		if ( get_post_type($order_id) == 'orders_inquiries' ) {
			update_post_meta($order_id, 'wp_dp_order_type', 'order');

			do_action('wp_dp_sent_order_email', $order_id);
			do_action('wp_dp_received_order_email', $order_id);
		}
	}
}

if(!function_exists('wp_dp_update_promotion_order')){
	function wp_dp_update_promotion_order($wp_dp_trans_id) {
		update_post_meta($wp_dp_trans_id, 'wp_dp_transaction_status', 'approved');
		$order_id = get_post_meta($wp_dp_trans_id, 'wp_dp_transaction_order_id', true);
		$wp_dp_promotions = get_post_meta($order_id, 'wp_dp_promotions', true);
		$listing_id = get_post_meta($order_id, 'wp_dp_listing_id', true);
		
		$promotions_saved = get_post_meta($listing_id, 'wp_dp_promotions', true);
		if ( ! empty($promotions_saved) ) {
			$wp_dp_promotions = array_merge($promotions_saved, $wp_dp_promotions);
		}
		
		update_post_meta($listing_id, 'wp_dp_promotions', $wp_dp_promotions);
		update_post_meta($order_id, 'wp_dp_order_status', 'approved');
		
		if ( ! empty($wp_dp_promotions) ) {
			foreach ( $wp_dp_promotions as $promotion_key => $promotion_array ) {
				update_post_meta($listing_id, 'wp_dp_promotion_' . $promotion_key, 'on');
				update_post_meta($listing_id, 'wp_dp_promotion_' . $promotion_key.'_expiry', $promotion_array['expiry']);
			}
		}    
	}
}

if ( isset($_POST['data']) && isset($_POST['signature']) ) {	
	//check post data
	$wp_dp_gateway_options = get_option('wp_dp_plugin_options');
	$private_key = $wp_dp_gateway_options['wp_dp_liqpay_private_key'];
	
	$data = $_POST['data'];
	$str = $private_key . $data . $private_key;
	$signature = base64_encode(sha1($str, 1));	
    
    if ( $_POST['signature'] == $signature ) {

		$data = json_decode(base64_decode($data), true);
		
		if ( $data['status'] == 'success' || $data['status'] == 'sandbox' ) {
		
			$wp_dp_id = $data['info'];

			$transaction_array = array();
			
			$transaction_array['wp_dp_trans_id'] = esc_attr($data['payment_id']);//Id платежу в системі LiqPay
			$transaction_array['wp_dp_post_id'] = esc_attr($data['info']);//ID listing post-type
			$transaction_array['wp_dp_transaction_status'] = 'approved';
			$transaction_array['wp_dp_order_time'] = esc_attr($data['create_date']);	
			$transaction_array['wp_dp_transaction_amount'] = esc_attr( $data['amount'] );
			$transaction_array['wp_dp_trans_currency'] = esc_attr($data['currency']);
			$transaction_array['wp_dp_signature'] = esc_attr($_POST['signature']);
			$transaction_array['wp_dp_liqpay_data'] = $data;
		
			$wp_dp_trans_id = isset($data['order_id']) ? $data['order_id'] : '';//ID transaction post-type
			
			$order_type = get_post_meta($wp_dp_trans_id, 'wp_dp_transaction_order_type', true);
			if ( $order_type == 'promotion-order' ) {
				wp_dp_update_promotion_order($wp_dp_trans_id);
				update_post_meta($wp_dp_trans_id, 'wp_dp_liqpay_data', $data);
			 } else {
				wp_dp_update_transaction($transaction_array, $wp_dp_trans_id);
				wp_dp_update_post($wp_dp_id, $wp_dp_trans_id);
			}
		}
	}
}
